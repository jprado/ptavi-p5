***** Análisis de una sesión SIP

Se ha capturado una sesión SIP con Ekiga (archivo sip.cap.gz), que se puede abrir con Wireshark. Se pide rellenar las cuestiones que se plantean en este guión en el fichero p5.txt que encontrarás también en el repositorio.

  * Observa que las tramas capturadas corresponden a una sesión SIP con Ekiga, un cliente de VoIP para GNOME. Responde a las siguientes cuestiones:
    * ¿Cuántos paquetes componen la captura?

	La captura está compuesta por 954 paquetes		

    * ¿Cuánto tiempo dura la captura?

	La captura tiene una duración de 56.149345 segundos

    * ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes?

	La IP de la máquina donde se ha realizado la captura es 192.168.1.34 se trata de una IP privada que pertenece al bloque de redes privadas de la clase C, que va de la dirección IP 192.168.0.0 a la 192.168.255.255, este grupo de redes está reservada para direcciones IP que crea cada router para usarse en una red doméstica.

  * Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el porcentaje del tráfico correspondiente al protocolo TCP y UDP.
    * ¿Cuál de los dos es mayor? ¿Tiene esto sentido si estamos hablando de una aplicación que transmite en tiempo real?

	UDP ocupa el 96.2% del tráfico intercambiado mientras que TCP ocupa el 2.1% por ello es mayor UDP. Este porcentaje tiene sentido ya que una sesión SIP como la que estamos analizando se basa en el protocolo UDP.

    * ¿Qué otros protocolos podemos ver en la jerarquía de protocolos? ¿Cuales crees que son señal y cuales ruido?

	También encontramos el Internet Control Message Protocol (ICMP), el Adress Resolution Protocol (ARP), el Session Initiation Protocol (SIP) el Real-Time Transport Protocol (RTP), el HyperText Transfer Protocol (HTTP) e incluso Session Description Protocol (SDP).

Considero el protocolo ICMP como ruido ya que solo nos da información sobre los problemas que hay en la red, también los paquetes DNS que preguntan por direcciones IP (y sus correspondientes respuestas) que corresponden a peticiones DNS que no tienen nada que ver con la sesion SIP que estamos analizando

  * Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, con una llamada entremedias.
    * Filtra por sip para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos?

	En el segundo 7 se envían 6 paquetes SIP, en el segundo 14 se envían 3 paquetes, el segundo 16 se envían 4 paquetes, en el segundo 38 y en el segundo 39 se envían 4 paquetes en cada segundo, y por último en el segundo 55 se envían otros 4 paquetes SIP

    * Y los paquetes con RTP, ¿cuándo se envían?

	En el segundo 17 se envían 39 paquetes RTP, en el segundo 18 se envían 51 paquetes, en el segundo 19 se envían 46 paquetes, en el segundo 20 se envían 20 paquetes, en el segundo 21 se envían 30 paquetes, en el segundo 23 se envían 60 paquetes, en el segundo 24 se envían 70 paquetes, en el segundo 25 se envían 90 paquetes, en el segundo 26 se envían 70 paquetes, en el segundo 27 se envían 49 paquetes, en el segundo 28 se envían 40 paquetes, en el segundo 29 se envían 67 paquetes, en el segundo 30 se envían 30 paquetes, en el segundo 31 se envían 10 paquetes, en el segundo 32 se envían 20 paquetes, en el segundo 33 se envían 30 paquetes, en el segundo 34 se envían 20 paquetes, en el segundo 35 y 36 se envían 30 paquetes respectivamente, en el segundo 37 se envían 10 paquetes.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Analiza las dos primeras tramas de la captura.
    * ¿Qué servicio es el utilizado en estas tramas?

	El Domain Name System (DNS) que se encarga de encontrar la dirección ip asociada a el nombre de una página web

    * ¿Cuál es la dirección IP del servidor de nombres del ordenador que ha lanzado Ekiga?

	La dirección IP del servidor de nombres es 80.58.61.250

    * ¿Qué dirección IP (de ekiga.net) devuelve el servicio de nombres?

	El servicio de nombres nos devuelve la dirección 86.64.162.35 que pertenece a Ekiga.net

  * A continuación, hay más de una docena de tramas TCP/HTTP.
    * ¿Podrías decir la URL que se está pidiendo?

	La URL completa es http://ekiga.net/ip/

    * ¿Qué user agent (UA) la está pidiendo?

	Ekiga

    * ¿Qué devuelve el servidor?

	El servidor devuelve la dirección IP: 83.36.48.212 que es la dirección privada de la máquina que está realizando la consulta

    * Si lanzamos el navegador web, por ejemplo, Mozilla Firefox, y vamos a la misma URL, ¿qué recibimos? ¿Qué es, entonces, lo que está respondiendo el servidor?

	Recibimos la dirección IP con la que estamos realizando la petición a Ekiga, la que dicha página web ve al recibir nuestra petición 

  * Hasta la trama 45 se puede observar una secuencia de tramas del protocolo STUN.
    * ¿Por qué se hace uso de este protocolo?

	Porque estamos tratando de averiguar si estamos detrás de un NAT o no

    * ¿Podrías decir si estamos tras un NAT o no?

	Sí, estamos detrás de un NAT porque haciendo un mapeado de puertos, la máquina de destino no es capaz de respondernos las peticiones que provienen del puerto 5061, es por eso que la máquina origen empieza a enviar peticiones con el puerto 5062 y la máquina destino puede responder

  * La trama 46 es la primera trama SIP. En un entorno como el de Internet, lo habitual es desconocer la dirección IP de la otra parte al realizar una llamada. Por eso, todo usuario registra su localización en un servidor Registrar. El Registrar guarda información sobre los usuarios en un servidor de localización que puede ser utilizado para localizar usuarios.
    * ¿Qué dirección IP tiene el servidor Registrar?

	Tiene la dirección IP: 86.64.162.35 

    * ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP?

	Los paquetes SIP se envían al puerto 5060

    * ¿Qué método SIP utiliza el UA para registrarse?

	El UA usa el método REGISTER para registrarse

    * Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA?

	En el campo 'Allow' vienen las instrucciones SIP que entiende el UA, y son las siguientes: INVITE, ACK, OPTIONS, BYE, CANCEL, NOTIFY, REFER, MESSAGE

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Fijémonos en las tramas siguientes a la número 46:
    * ¿Se registra con éxito en el primer intento?

	No, en el primer intento no se registra con éxito (puede ser porque el register está mal hecho y los \r\n aparecen separados o porque el protocolo es así y al primer intento de REGISTER nos da una respuesta de Unauthorized y tenemos que repetir el proceso para conseguir registrarnos

    * ¿Cómo sabemos si el registro se ha realizado correctamente o no?

	Porque el servidor de register nos envía un SIP 200 OK, señal de que nos hemos registrado con éxito

    * ¿Podrías identificar las diferencias entre el primer intento y el segundo de registro? (fíjate en el tamaño de los paquetes y mira a qué se debe el cambio)

	El primer REGISTER tiene 523 bytes de tamaño mientras que el segundo tiene 712 bytes de tamaño. Aparte, en el primer REGISTER no se incluye el campo Authorization en la cabecera de SIP que es necesario para poder registrarse con éxito, en el segundo REGISTER si que aparece el cambo Authorization en la cabecera de SIP

    * ¿Cuánto es el valor del tiempo de expiración de la sesión? Indica las unidades.

	El tiempo de expiración es de 3600 segundos

  * Una vez registrados, podemos efectuar una llamada. Vamos a probar con el servicio de eco de Ekiga que nos permite comprobar si nos hemos conectado correctamente. El servicio de eco tiene la dirección sip:500@ekiga.net. Veamos el INVITE de cerca.
    * ¿Puede verse el nombre del que efectúa la llamada, así como su dirección SIP?

	Si, se puede ver el nombre de quien efectúa la llamada en el campo 'From' de la cabecera del mensaje, "Gregorio Robles" así como su dirección SIP <sip:grex@ekiga.net>, esta información viene más detallada en los campos 'SIP Display info' y 'SIP from address' del 'From'

    * ¿Qué es lo que contiene el cuerpo de la trama? ¿En qué formato/protocolo está?

	Contiene una petición de llamada o lo que parecen los datos que la describirían ya que podemos encontrar información sobre la tasa de muestreo, la codificación usada para realizar la llamada y demás datos como protocolos y formatos. Todo esto está dentro del protocolo SDP

    * ¿Tiene éxito el primer intento? ¿Cómo lo sabes?

	El primer intento no tiene éxito ya que el servidor le devuelve un error 407 Proxy Authentication Required y el cliente envía un ACK con la intención de repetir el INVITE

    * ¿En qué se diferencia el segundo INVITE más abajo del primero? ¿A qué crees que se debe esto?

	La primera diferencia que encontramos es el tamaño de la trama enviada, el primer INVITE tiene una longitud de 982 bytes mientras que el segundo tiene una longitud de 1181 bytes. Esta diferencia se debe a que ahora aparece en la cabecera un campo llamado 'Proxy-Authorization' que es necesaria para poder realizar la llamada

  * Una vez conectado, estudia el intercambio de tramas.
    * ¿Qué protocolo(s) se utiliza(n)? ¿Para qué sirven estos protocolos?

	Se utilizan los protocolos H.261, RTP y RTCP. El protocolo H.261 es un codificador de video para servicios audiovisuales de 64 kbit/s, por lo que su principal función será permitirnos mandar información audiovisual hacia el otro lado de la comunicación. Por otro lado, RTP es un protocolo que define un formato de paquete estándar para envío de audio y video sobre Internet. Finalmente el protocolo RTCP sirve para supervisar las estadísticas de transmisión y calidad de servicio y ayuda a la sincronización de multiples flujos. 

    * ¿Cuál es el tamaño de paquete de los mismos?

	El tamaño de paquete de H.261 va variando así que no podría definir un tamaño fijo. El tamaño de los paquetes RTP al contrario sí que es fijo, ocupan 214 bytes contando cabeceras y cuerpo del mensaje. Por último, los paquetes RTCP tienen un tamaño de 102 bytes cuando contienen algún error y 43 bytes cuando indican que el tráfico fluye correctamente. 	

    * ¿Se utilizan bits de padding?

	No, en ningún momento se utilizan bits de padding, este campo siempre está a False por ello no hay ningún bit de relleno en esas tramas.

    * ¿Cuál es la periodicidad de los paquetes (en origen; nota que la captura es en destino)?

	La periodicidad es de aproximadamente 0,020 segundos en origen, esto lo calculamos fijandonos en el valor de timestamp, de ahí deducimos que se envían 50 paquetes por segundo, el periodo lo sacamos de la siguiente forma: 1/50 = 0,02 s

    * ¿Cuántos bits/segundo se envían?

	Mediante un cálculo tomando el tiempo y multiplicando por la muestras por segundo se envían 64 kbits/segundo	

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Vamos a ver más a fondo el intercambio RTP. En Telephony hay una opción RTP. Empecemos mirando los flujos RTP.
    * ¿Cuántos flujos hay? ¿por qué?

	Hay dos flujos RTP, ambos tienen puertos de origen y destino distintos, cada uno tiene una carga, g711a es un estándar para codificación de audio, que es distinto de h.261 que he explicado anteriormente, por ello tienen que ir en flujos RTP distintos. 

    * ¿Cuántos paquetes se pierden?

	Según la columna Lost no se pierden paquetes (0%)

    * ¿Cuál es el valor máximo del delta? ¿Y qué es lo que significa el valor de delta?

	El valor máximo para el primer flujo (g711a) es de 1290.444 ms mientras que para el segundo flujo (h.261) es de 1290.479. Estos valores representan el retardo máximo en la tasa de envío que se ha experimentado durante la llamada.

    * ¿Cuáles son los valores de jitter (medio y máximo)? ¿Qué quiere decir eso? ¿Crees que estamos ante una conversación de calidad?

	Los valores medios de jitter son 42.500 ms para el primer flujo y 153.240 ms para el segundo. Los valores máximos de jitter son 119.635 ms para el primer flujo y 183.096 ms para el segundo flujo. Estos valores se refieren al tiempo medio y máximo de retraso en la recepción que hay entre los paquetes de esos flujos. Creo que estamos en una conversación de calidad puesto que no se han perdido paquetes, lo que quiere decir que todos han llegado a tiempo y no se han perdido datos de la llamada por llegar más tarde de lo previsto


  * Elige un paquete RTP de audio. Analiza el flujo de audio en Telephony -> RTP -> Stream Analysis.
    * ¿Cuánto valen el delta y el jitter para el primer paquete que ha llegado?

	Ambos valen 0 ms ya que al ser el primer paquete de este tipo aún no se han podido calcular los valores de dichos campos

    * ¿Podemos saber si éste es el primer paquete que nos han enviado?

	Sí, de varias formas, por un lado en la columna Packet podemos ver que tiene el numero menor de toda esa tabla, también podemos deducirlo al ver que los valores Delta y Jitter no están calculados aún y por último en la columna Marker observamos que hay un puntito indicando que es el primero.

    * Los valores de jitter son menores de 10ms hasta un paquete dado. ¿Cuál?

	Los valores de jitter son menores de 10 ms hasta el paquete 247

    * ¿A qué se debe el cambio tan brusco del jitter?

	Se debe a que por alguna razón el paquete se ha retrasado mucho (ha tardado en llegar a su destino) con respecto a los demás paquetes enviados anteriormente

    * ¿Es comparable el cambio en el valor de jitter con el del delta? ¿Cual es más grande?

	Sí, ambos han aumentado considerablemente, dado que Delta es la tasa de envío de mensajes y por alguna razón el paquete 247 ha tardado mucho en enviarse. Es más grande el valor de Delta puesto que el valor de jitter se calcula operando ese valor de Delta y siempre saldrá menor

  * En Telephony selecciona el menú VoIP calls. Verás que se lista la llamada de voz IP capturada en una ventana emergente. Selecciona esa llamada y pulsa el botón Play Streams.
    * ¿Cuánto dura la conversación?

	La conversación dura 24 segundos, lo podemos ver antes de entrar a 'Play Streams' si nos fijamos en la columna 'Duration'

    * ¿Cuáles son sus SSRC? ¿Por qué hay varios SSRCs? ¿Hay CSRCs?

	Sus SSRC son dos: 0xbf4afd37 que identifica la fuente desde la que se han enviado los archivos de audio (g711a), y por otro lado 0x43306582 que identifica a la fuente desde la que se han enviado los archivos de audio y video (h.261). Hay dos SSRCs puesto que cada flujo de datos se ha enviado desde un puerto distinto y el SSRC nos ayuda a distinguir la fuente desde la que se han enviado. No hay CSRCs puesto que estos sirven para identificar las fuentes cuando varias de ellas contribuyen a generar un único flujo.

  * Identifica la trama donde se finaliza la conversación.
    * ¿Qué método SIP se utiliza?

	Utiliza el método BYE 

    * ¿En qué trama(s)?

	Las tramas en las que se intenta finalizar la conversación por parte del cliente son la 924, 925, 927, 933 y por parte del servidor (los mensajes 200 OK) se envían en las tramas: 938, 939, 940 y 941

    * ¿Por qué crees que se envía varias veces?

	Las tres tramas que se reenvían son copias de la primera (924) como pone al analizar el campo Request Line del protocolo SIP

  * Finalmente, se cierra la aplicación de VozIP.
    * ¿Por qué aparece una instrucción SIP del tipo REGISTER?

	Porque REGISTER también sirve para darse de baja en una sesión SIP si ponemos el campo Expires a '0'

    * ¿En qué trama sucede esto?

	Esto sucede en las tramas 950 y 952 ya que se repite el mismo proceso que cuando nos queremos registrar, primero mandamos la petición, el servidor nos devuelve el error 401 Unauthorized y el cliente vuelve a enviar la petición pero ahora incluye el campo de la cabecera 'Authorization'

    * ¿En qué se diferencia con la instrucción que se utilizó con anterioridad (al principio de la sesión)?

	Se diferencia en que ahora el campo Expires tiene el valor '0' de modo que el servidor elimina el registro que tenía de dicha dirección de su base de datos, en la petición de registro el campo Expires tenía el valor de '3600' de modo que quedaría registrado por una hora.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

***** Captura de una sesión SIP

  * Dirígete a la web de IPTel (https://serweb.iptel.org/user/reg/index.php) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte.

  * Lanza Ekiga, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú ``Editar'' y seleccionar ``Cuentas''. Tendrás que rellenar los campos de manera similar a lo que se puede ver en la imagen a continuación (recuerda poner tu nombre de usuario en lugar de ``grex''). Comprueba que estás conectado (En la barra al final de la ventana podrás ver ``Registrado''). Al terminar, cierra completamente Ekiga.
(en el guión en PDF en este punto podrás ver una gráfica)

  * Captura una sesión SIP de una conversación con el número SIP sip:music@sip.iptel.org. Recuerda que has de comenzar a capturar tramas antes de arrancar Ekiga para ver todo el proceso.

  * Observa las diferencias en el inicio de la conversación entre el entorno del laboratorio y el del ejercicio anterior:
    * ¿Se utilizan DNS y STUN? ¿Por qué?

	Se utilizan DNS para para preguntar por la dirección de IPTel, en mi captura no se puede ver puesto que ya tenía la dirección guardada en la caché. No se utiliza el protocolo STUN ya que al hacer la captura desde le ordenador de la universidad estamos usando una direción IP pública al estar conectados a la red por LAN, en caso de hacer la captura desde el ordenador portátil sí que aparecería el protocolo STUN al estar usando una dirección IP privada en la red inalámbrica. 

    * ¿Son diferentes el registro y la descripción de la sesión?

	Los registros son iguales (también requerimos de una autorización para poder registrarnos) en todo salvo que en mi caso el campo expires tenía valor 60 segundos. En cuanto a la descripción de la sesión, a mi me deja realizar varias opciones que en la anterior captura no aparecían, como: SUBSCRIBE, INFO, PING, PRACK

  * Identifica las diferencias existentes entre esta conversación y la conversación anterior:
    * ¿Cuántos flujos tenemos?

	Ahora tenemos tres flujos, dos en la dirección de mi ordenador hacia el servidor y uno desde el servidor hacia mi ordenador, los tres usan el protocolo Speex en la columna Payload.

    * ¿Cuál es su periodicidad?

	Teniendo en cuenta que se han enviado 1602 paquetes en 32 segundos, obstenemos que se han enviado 50 paquetes por segundo, para calcular el periodo usamos la misma fórmula de antes T = 1/50 = 0,02 segundos

    * ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter?

	En el flujo de ida correspondiente a la llamada tenemos los siguientes valores: Max Delta = 33.326 ms, Max Jitter = 2.123 ms y Mean Jitter = 0.251 ms. Por otro lado en el flujo de vuelta hacia mi ordenador tenemos los siguientes valores: Max Delta = 39.966 ms, Max Jitter = 4.974 ms y Mean Jitter = 0.218 ms 

    * ¿Podrías reproducir la conversación desde Wireshark? ¿Cómo? Comprueba que poniendo un valor demasiado pequeño para el buffer de jitter, la conversación puede no tener la calidad necesaria.

	Sí, se puede reproducir la conversación desde Wireshark yendo a la pestaña Telephony -> RTP -> RTP Streams -> seleccionamos el flujo de vuelta -> Analyze -> Play Streams
	En mi caso no puedo reproducir el audio ya que mi ordenador no tiene los codecs necesarios para hacerlo.

    * ¿Sabrías decir qué tipo de servicio ofrece sip:music@iptel.org?

	Ofrece un 'contestador de voz' que reproduce música al ser llamado

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Filtra por los paquetes SIP de la captura y guarda *únicamente* los paquetes SIP como p5.pcapng. Abre el fichero guardado para cerciorarte de que lo has hecho bien. Deberás añadirlo al repositorio.

[Al terminar la práctica, realiza un push para sincronizar tu repositorio GitLab]
